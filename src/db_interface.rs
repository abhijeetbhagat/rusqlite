extern crate libc;

use self::libc::*;
use std::c_str;
use std::ptr;
use db_structs::*;
use ffi::*;
use std::vec;
use std::str;
use std::mem;
use std::rc::Rc;

pub struct sqlite3;

pub struct null;

pub struct Connection{
    path : &'static str,
    pdb : *mut dbh,
    pArg : *mut c_void,
    stmt : Rc<*mut sqlite3_stmt> //stmt is a member because we want to finalize it if the connection
        //is closed before stepping completes
    //cur : Option<Box<Cursor>>
}

pub struct Cursor<'a>{
    conn : &'a mut Connection,
    stmt : *mut sqlite3_stmt,
    pzTail :*const c_char
}

pub struct Row{
    stmt : *mut sqlite3_stmt
}

impl sqlite3{
    pub fn connect(db_file : &'static str)->Connection{
        //let mut _conn = 
        Connection { path : db_file, 
                     pdb : ptr::null_mut(), 
                     pArg : ptr::null_mut(),
                     stmt : Rc::new(ptr::null_mut())
        }
                     //cur : None};
        // _conn.cur = Some(box Cursor{ conn : _conn, stmt : ptr::null_mut(), pzTail : ptr::null() });
        // return _conn;
    }
}

extern fn cmpare (vp : *mut c_void, i1 : c_int, str1 : *const c_void, i2 : c_int, str2 : *const c_void)->c_int{
    println!("dd");
    // unsafe{
    //     let s1 = c_str::CString::new(str1 as *const c_char, false).to_string();// mem::transmute(str1);
    //     let s2 = c_str::CString::new(str2 as *const c_char, false).to_string();//mem::transmute(str2);
    //     return match s1.cmp(&s2){
    //         Less => -1,
    //         Equal => 0,
    //         _ => 1
    //     }
    // }
    1 
}

extern fn destroy(vp: *mut c_void){
}


impl Connection{
    pub fn cursor(&mut self) -> Result<Cursor, String>{
        unsafe{        
            match sqlite3_open(self.path.to_c_str().as_ptr(), &mut self.pdb){
                SQLITE_OK => Ok(Cursor{ conn : self, stmt : ptr::null_mut(), pzTail : ptr::null() }),
                _  => Err("Error".to_string()),
            }
        }
    }

    pub fn close(&mut self){
        unsafe{
            match sqlite3_finalize(*(self.stmt.deref())){
                SQLITE_OK => println!("finalization succeeded"),
                _ => println!("finalization failed")
            }
            match sqlite3_close(self.pdb){
                SQLITE_OK => println!("Successfully closed the connection"),
                SQLITE_BUSY => println!("DB is busy because a prepared stmt wasn't closed"),
                _ => println!("Connection could not be closed for some reason")
            }
        }
    }
    //TODO: this isnt working. fix it.
    pub fn create_collation(&mut self, name : &'static str/*, callable : fn(str1:String, str2: String)->i32*/){
        //name.with_c_str(|_s|{
        unsafe{
            sqlite3_create_collation(self.pdb, name.as_ptr() as *const _, SQLITE_UTF8 as i32, ptr::null_mut(), cmpare);
        }
        //});
    }
}

//TODO: find a way to iterate over the argv, azColName arrays
extern fn callback(vp : *mut c_void, argc: c_int, argv : *mut*mut c_char, azColName : *mut*mut c_char)->c_int{
    let mut argvc : *const*const c_char = argv as *const*const c_char;
    let mut argcolc : *const*const c_char = azColName as *const*const c_char;
    if argvc as uint == 0{
        
    }else{
        unsafe{
            while *argvc != 0 as *const _{
                let ep = c_str::CString::new(*argvc, false).as_bytes_no_nul().to_vec();
                let cn = c_str::CString::new(*argcolc, false).as_bytes_no_nul().to_vec();
                println!("{} - {}", String::from_utf8(cn), String::from_utf8(ep));
                argvc = argvc.offset(1);
                argcolc = argcolc.offset(1);
            }
        }
    }
    0 as i32
   
}

//the idea was something like this:
//execute_many(c, [p1, p2, p3], [o1, o2, o3, o4])
//seems that p1, p2, ... cant be passed around like that
#[macro_export]
macro_rules! execute_many(
    ($o:expr, [$($query:ident)|+])  => (
        $(
        println!("{}",$o.$query);
            )+
        )
)

impl<'a> Cursor<'a>{
    pub fn finalize(&mut self){
        unsafe{
            sqlite3_finalize(self.stmt);
        }
    }
    pub fn execute(&mut self, query : &'static str)->Result<(), String>{
        unsafe{
            match sqlite3_prepare_v2(self.conn.pdb,
                                     query.to_c_str().as_ptr(),
                                     query.len() as i32,
                                     &mut self.stmt,
                                     &mut self.pzTail){
                SQLITE_OK =>{
                    self.conn.stmt = Rc::new(self.stmt);
                    Ok(())
                },
                _ => Err("execute: Error".to_string())
            }
           
        }
    }

    pub fn execute_with_finalize(&mut self, query: &'static str)->Result<(), String>{
        unsafe{
            match sqlite3_exec(self.conn.pdb,
                         query.to_c_str().as_ptr(),
                         callback,
                         ptr::null_mut(),
                         ptr::null_mut()){
                SQLITE_OK => Ok(()),
                _ =>  Err("execute_with_finalize: Error".to_string())
            }
        }
    }

    pub fn executescript(&mut self, query : &'static str)->Result<(), String>{
        unsafe{
            let mut pzTail:*mut*const c_char = ptr::null_mut();
            let mut q = query.to_string();
            loop{
                match sqlite3_prepare_v2(self.conn.pdb, 
                                         q.to_c_str().as_ptr(),
                                         q.len() as i32,
                                         &mut self.stmt, 
                                         mem::transmute(&mut pzTail)
                                         ){
                    SQLITE_OK => {
                        q = c_str::CString::new(mem::transmute(pzTail.offset(0)), false).to_string();
                        match sqlite3_step(self.stmt){
                            SQLITE_DONE => {
                                sqlite3_finalize(self.stmt);
                                if q.len() == 0 {return Ok(())}
                                else {continue}
                            }, 
                            _ => return Err("executescript: Error".to_string())
                        }
                    },
                    _ => { return Err("executescript: Error".to_string());}
                }
                sqlite3_finalize(self.stmt);
            }
        }
    }
    
    //compiler will complain saying theres no iter() defined for &[T] since T
    //is a trait not a type; hence, since theres no size, it cant figure it out
    //by what size it has to jump to the next element. 
    //However, for pointers, theres a fixed size. So &[&T] is good enough
    pub fn execute_bind(&mut self, query : &'static str, values : &[&Bindable]){
        //prepare -> bind -> step
        unsafe{
            match sqlite3_prepare_v2(self.conn.pdb, 
                                     query.to_c_str().as_ptr(),
                                     query.len() as i32,
                                     &mut self.stmt, 
                                     ptr::null_mut()
                                     ){
                SQLITE_OK => {
                    let mut i = 1; //index to sqlite3_bind_x() starts with 1
                    for value in values.iter(){
                        value.bind_at(&i, &mut *self.stmt);
                        i += 1;
                    }

                    match sqlite3_step(self.stmt){
                        SQLITE_DONE => {
                            println!("Done");},
                        _ =>  {println!("Error");}
                    }
                },
                _ => {}
            }
            sqlite3_finalize(self.stmt);
        }
    }

    pub fn execute_many(&mut self, query : &'static str, values : &[&[&Bindable]]){
        unsafe{
            for c in values.iter(){ 
                match sqlite3_prepare_v2(self.conn.pdb, 
                                         query.to_c_str().as_ptr(),
                                         query.len() as i32,
                                         &mut self.stmt, 
                                         ptr::null_mut()
                                         ){
                    SQLITE_OK => {
                        
                        let mut i = 1; //index to sqlite3_bind_x() starts with 1
                        for value in c.iter(){
                            value.bind_at(&i, &mut *self.stmt);
                            i += 1;
                        }

                        match sqlite3_step(self.stmt){
                            SQLITE_DONE => {
                                //sqlite3_finalize(self.stmt);
                                //continue;
                            },
                            _ =>  {panic!("execute_many: Error");}
                        } 
                    },
                    _ => {panic!("execute_many: Error");}
                } 
                sqlite3_finalize(self.stmt); //we are done with all the elements
            } 
        } 
    }

    // fn bind_value<T : Bindable>(&mut self, value : &T, i : &i32){
    //     Bindable::bind(value, i, &*self.stmt);
    // }

    pub fn fetchone_as_vec(&mut self)->Option<Vec<Option<String>>>{
        self.step()
    }
    
    pub fn fetchone_as_row(&mut self) -> Option<Row>{
        self.step_as_row()
    }

    //pub fn fetchmany(&mut self, )
 
    fn step(&mut self)->Option<Vec<Option<String>>>{
        let mut v = vec![];
        unsafe{
            match sqlite3_step(self.stmt){
                SQLITE_DONE => {
                    sqlite3_finalize(self.stmt);
                    return None;
                },
                SQLITE_ROW =>  {
                    for i in range(0i32, sqlite3_column_count(self.stmt)){
                        let col_name = sqlite3_column_text(self.stmt, i);
                        if col_name.is_null(){
                            v.push(None);
                            continue;
                        }
                        
                        let name = c_str::CString::new(col_name, false); 
                        v.push(Some(name.to_string()));
                    }
                    return Some(v)
                },
                _ => return None
            }
        }
    }

    fn step_as_row(&mut self)->Option<Row>{
        unsafe{
            match sqlite3_step(self.stmt){
                SQLITE_DONE => {
                    sqlite3_finalize(self.stmt);
                    return None;
                },
                SQLITE_ROW =>  {
                    Some(Row{stmt : self.stmt})
                },
                _ => None
            }
        }
    }

    pub fn fetchall(&mut self)->Vec<Vec<Option<String>>>{
        let mut v = Vec::new();
        loop{
            match self.step(){ //we are not borrowing, hence no &...ref...
                Some(row) => v.push(row), //without cloning, all rows will be same
                _ => break
            }
        }
        v
    }

    pub fn fetchmany(&mut self, size : i32)->Vec<Vec<Option<String>>>{
        let mut v = Vec::new();
        for i in range(0, size){
            match self.step(){
                Some(row) => v.push(row),
                _ => break
            }
        }
        v
    }
}

impl Row{
    pub fn get_col_data<T : Extractable>(&mut self, i : i32)->T{
        Extractable::get_val(self, i)
    }

    pub fn keys(&mut self) -> Option<Vec<String>>{
        let mut col_names = Vec::new();
        unsafe{
            let col_count =  sqlite3_column_count(self.stmt);
            if col_count == 0{
                return None;
            }
            for i in range(0i32, col_count){
                let col_name = sqlite3_column_name(self.stmt, i);
                let name = c_str::CString::new(col_name, false);
                col_names.push(name.to_string());
            }
        }
        Some(col_names)
    }
}

pub trait Extractable{
    fn get_val(row : &mut Row, i : i32)->Self;
}

impl Extractable for i32{ //int
    fn get_val(row : &mut Row, i : i32)->i32{
        unsafe{
            sqlite3_column_int(row.stmt, i)
        }
    }
}

impl Extractable for f64{ //double
    fn get_val(row : &mut Row, i : i32)->f64{
        unsafe{
            sqlite3_column_double(row.stmt, i)
        }
    }
}

impl Extractable for String{ //string
    fn get_val(row : &mut Row, i : i32)->String{
        unsafe{
            let col_name = sqlite3_column_text(row.stmt, i);
            let name = c_str::CString::new(col_name, false);
            name.to_string()
        }
    }
}

pub trait Bindable{
  fn bind_at(&self, i : &i32, stmt : &mut sqlite3_stmt);
}

impl Bindable for i32{
    fn bind_at(&self, i : &i32, stmt : &mut sqlite3_stmt){
        unsafe{
            sqlite3_bind_int(stmt, *i, *self);
        } 
    }
}

impl Bindable for f64{
    fn bind_at(&self, i : &i32, stmt : &mut sqlite3_stmt){
        unsafe{
            println!("binding {}", self);
            sqlite3_bind_double(stmt, *i, *self);
        }
    }
}

impl Bindable for String{
    fn bind_at(&self, i : &i32, stmt : &mut sqlite3_stmt){
        let l = self.len() as c_int;
        self.as_slice().with_c_str(|_s|{
            unsafe{
                sqlite3_bind_text(stmt, 
                                  *i,
                                  self.as_slice().as_ptr() as *const _,
                                  self.len() as i32, 
                                  destroy);
            }
        });
        // unsafe{
        //     println!("{}", self.as_slice());
        //     sqlite3_bind_text(stmt, *i, self.as_slice().to_c_str().as_ptr(), l, destroy);
        // }
    }
}

impl<'a> Bindable for &'a [u8]{
    fn bind_at(&self, i : &i32, stmt : &mut sqlite3_stmt){
        unsafe{
            sqlite3_bind_blob(stmt, *i, mem::transmute(self), self.len() as i32,destroy);
        }
    }
}

impl<'a> Bindable for &'a str{
    fn bind_at(&self, i : &i32, stmt : &mut sqlite3_stmt){
        self.with_c_str(|_cstr|{
            unsafe{
                sqlite3_bind_text(stmt, *i,_cstr ,10i32, destroy);
            }
        });
        
    }
}


// impl<T> Bindable for Option<T>{
//     fn bind_at(&self, i : &i32, stmt : &mut sqlite3_stmt){
//         unsafe{
//             sqlite3_bind_null(stmt, *i);
//         }
//     }
// }

impl Bindable for null{
    fn bind_at(&self, i : &i32, stmt : &mut sqlite3_stmt){
        unsafe{
            sqlite3_bind_null(stmt, *i);
        }
    }
}
