pub enum dbh{
}

pub enum sqlite3_stmt{
}

pub enum sqlite3_context{}

pub enum sqlite3_value{}

pub enum QueryResult{
    SQLITE_OK = 0,
    SQLITE_ERROR = 1,
    SQLITE_INTERNAL = 2,
    SQLITE_ROW = 100,
    SQLITE_DONE = 101,
    SQLITE_BUSY = 5,
    SQLITE_MISUSE = 21
}

pub enum Callback{
}

pub enum TextEncodings{
    SQLITE_UTF8 = 1,
    SQLITE_UTF16LE = 2,
    SQLITE_UTF16BE = 3,
    SQLITE_UTF16 = 4, /*Use native byte order*/
    SQLITE_ANY = 5, /*Deprecated*/
    SQLITE_UTF16_ALIGNED = 8 /*sqlite3_create_collation only*/
}
