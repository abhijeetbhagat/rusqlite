#![feature(globs)]

#![allow(unused_must_use, dead_code)]

#![feature(phase, macro_rules)]
#[phase( plugin, link)] 
extern crate rusqlite3;

use std::fmt;
use rusqlite3::db_interface::*;

#[deriving(Show)]
struct Wine{
    id : i32,
    name : String,
    company : String,
    country : String
}

// impl fmt::Show for Wine{
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
//         write!(f,
//                "id : {}, name : {}, company : {}, country : {}", 
//                self.id, 
//                self.name, 
//                self.company, 
//                self.country)
//     }
// }

//#[start]
fn main(){
    let mut conn = sqlite3::connect(":memory:");
    //conn.create_collation("reverse");
    match conn.cursor(){
        Ok(mut c) => {
            //c.execute_with_finalize("create table test(x, y)");
            c.executescript("create table test(x, y)");
            /*match c.executescript("insert into test(x) values('a'); insert into test(x) values('b');"){
                Err(e) => println!("{}", e),
                _ => {}
            }*/
            //let v: Option<i32> = None;
            //c.execute_bind("insert into test(x,y) values (?,?)", &[&1i32, &String::from_str("abhi")]);
            c.execute_many("insert into test(x,y) values (?,?)", //&[&[&1i32, &null],
                           &[&[&1i32,&String::from_str("abhi")],
                             &[&2i32,&("ahmed").to_string()]]);

            c.execute("select x, y from test");
            let r = c.fetchone_as_row();
            for key in r.unwrap().keys().unwrap().iter(){
                println!("{}", key);
            }
            //c.executemany("insert into test(x) values ({:s})", [("a"), ("b")]);
            //c.execute_with_finalize("select x, y from test");
            // c.execute("select x, y from test");

            // for cell in c.fetchone_as_vec().unwrap().iter(){
            //     match cell{
            //         &Some(ref v)=>println!("{}", v),
            //         _ => println!("")
            //     }
            // }
            // for cell in c.fetchone_as_vec().unwrap().iter(){
            //     match cell{
            //         &Some(ref v)=>println!("{}", v),
            //         _ => println!("")
            //     }
            // }
        },
        Err(e)=> println!("{}", e)
        
    }

    conn.close();
    // let rows = c.fetchall();//fetchone();
    // for row in rows.iter(){
    //     for cell in row.iter(){
    //         match cell{ //cell is borrowed, thats why compiler wants us to specify &...ref...
    //             &Some(ref c) => println!("{}", c),
    //             &None => println!("NULL")
    //         }
    //     }
        
    // }
    
    // c.execute("insert into wines values (4, 'sula', 'baramati', 'india')");
    // c.fetchone_as_row();
    
    // for e in  c.fetchone_as_vec().unwrap().iter(){
    //     println!("{}", e);
    // }
    
    // let row = c.fetchone_as_row();
    // let w = Wine { 
    //     id : row.unwrap().get_col_data(0),
    //     name : row.unwrap().get_col_data(2),
    //     company : row.unwrap().get_col_data(1),
    //     country : row.unwrap().get_col_data(3),
    // };
    // println!("{}", w);
    
    // c.executescript("create table employee(
    //           firstname,
    //           lastname,
    //           age
    //           );");

    // create table book(
    //     title,
    //     author,
    //     published
    // );

    // insert into book(title, author, published)
    // values (
    //     'Dirk Gently''s Holistic Detective Agency',
    //     'Douglas Adams',
    //     1987
    // );");
    //c.fetchone_as_row();
}

