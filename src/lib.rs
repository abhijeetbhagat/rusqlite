#![feature(globs)]
#![feature(macro_rules)]
#![crate_name="rusqlite3"]
#![crate_type="lib"]

pub mod db_interface;
pub mod db_structs;
pub mod ffi;
