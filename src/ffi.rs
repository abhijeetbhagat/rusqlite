extern crate libc;

use self::libc::*;
use db_structs::*;

#[link(name="sqlite3")]
extern{
    pub fn sqlite3_open(filename : *const c_char, ppdb : *mut*mut dbh)->QueryResult;
    pub fn sqlite3_close(pdb: *mut dbh)->QueryResult;
    //pub fn sqlite3_close_v2(pdb: *mut dbh)->QueryResult;
    pub fn sqlite3_prepare(pdb : *mut dbh, 
                           zSql : *const c_char, 
                           nByte : c_int, 
                           ppStmt : *mut*mut sqlite3_stmt,
                           pzTail : *mut*const c_char)->QueryResult;
    pub fn sqlite3_prepare_v2(pdb : *mut dbh, 
                              zSql : *const c_char, 
                              nByte : c_int, 
                              ppStmt : *mut*mut sqlite3_stmt,
                              pzTail : *mut*const c_char)->QueryResult;
    pub fn sqlite3_step(stmt : *mut sqlite3_stmt)->QueryResult;
    pub fn sqlite3_finalize(stmt : *mut sqlite3_stmt)->QueryResult;
    pub fn sqlite3_exec(pdb: *mut dbh,
                        sql :*const c_char,
                        cb : extern fn (vp : *mut c_void,
                                        argc: c_int
                                        , argv : *mut*mut c_char, 
                                        azColName : *mut*mut c_char)->c_int,
                        vp : *mut c_void,
                        errmsg : *mut*mut c_char)->QueryResult;
    pub fn sqlite3_column_count(stmt: *mut sqlite3_stmt)->c_int;
    pub fn sqlite3_column_text(stmt: *mut sqlite3_stmt, iCol : c_int) -> *const c_char;
    pub fn sqlite3_column_int(stmt: *mut sqlite3_stmt, iCol : c_int)->c_int;
    pub fn sqlite3_column_double(stmt: *mut sqlite3_stmt, iCol : c_int)->c_double;
    pub fn sqlite3_column_name(stmt: *mut sqlite3_stmt, n : c_int) -> *const c_char;
    pub fn sqlite3_create_collation(pdb: *mut dbh,
                                    zName : *const c_char,
                                    eTextRep : c_int, 
                                    pArg : *mut c_void, 
                                    xCompare : extern fn (vp : *mut c_void,
                                                          i1 : c_int,
                                                          str1 : *const c_void,
                                                          i2 : c_int, 
                                                          str2 : *const c_void)->c_int) -> c_int;
    pub fn sqlite3_create_collation_v2(pdb: *mut dbh, 
                                       zName : *const c_char,
                                       eTextRep : c_int, 
                                       pArg : *mut c_void,
                                       xCompare : extern fn (vp : *mut c_void,
                                                             i1 : c_int, 
                                                             str1 : *const c_void, 
                                                             i2 : c_int, 
                                                             str2 : *const c_void)->c_int, 
                                       xDestroy : extern fn(vp: *mut c_void)) -> c_int;
    
    pub fn sqlite3_bind_double(stmt: *mut sqlite3_stmt, i : c_int, val : c_double)-> c_int;
    pub fn sqlite3_bind_int(stmt: *mut sqlite3_stmt, i : c_int, val : c_int) -> c_int;
    pub fn sqlite3_bind_text(stmt: *mut sqlite3_stmt, 
                             i : c_int, 
                             val :*const c_char, 
                             nBytes : c_int,
                             destructor : extern fn (param : *mut c_void)) -> c_int;
    pub fn sqlite3_bind_null(stmt: *mut sqlite3_stmt, i : c_int)->c_int;
    pub fn sqlite3_bind_blob(stmt: *mut sqlite3_stmt, 
                             i : c_int, 
                             val :*const c_void, 
                             nBytes : c_int,
                             destructor : extern fn (param : *mut c_void)) -> c_int;
    pub fn sqlite3_create_function(db: *mut dbh,
                                   zFunctionName: *const c_char,
                                   nArg: c_int,
                                   eTextRep: c_int,
                                   pApp :*mut c_void,
                                   xFunc :extern fn (ctx: *mut sqlite3_context,
                                                     iArg : c_int,
                                                     val : *mut*mut sqlite3_value),
                                   xStep : extern fn(ctx: *mut sqlite3_context,
                                                     iArg : c_int,
                                                     val : *mut*mut sqlite3_value),
                                   xFinal : extern fn(ctx: *mut sqlite3_context))->c_int;
    pub fn sqlite3_create_function_v2(db: *mut dbh,
                                   zFunctionName: *const c_char,
                                   nArg: c_int,
                                   eTextRep: c_int,
                                   pApp :*mut c_void,
                                   xFunc :extern fn (ctx: *mut sqlite3_context,
                                                     iArg : c_int,
                                                     val : *mut*mut sqlite3_value),
                                   xStep : extern fn(ctx: *mut sqlite3_context,
                                                     iArg : c_int,
                                                     val : *mut*mut sqlite3_value),
                                   xFinal : extern fn(ctx: *mut sqlite3_context),
                                   xDestroy : extern fn (param : *mut c_void))->c_int; 
}
