#![feature(globs)]
#![feature(phase)]
#![allow(unused_must_use, dead_code, unused_variables,unused_imports)]

extern crate rusqlite3;
extern crate test;
#[phase(plugin, link)] extern crate log;
use std::fmt;
use rusqlite3::db_interface::*;

#[test]
fn test_fetchmany(){
    let mut conn = sqlite3::connect(":memory:");
    match conn.cursor(){
        Ok(mut c) => {
            c.executescript("create table test(x, y)");
            c.execute_many("insert into test(x,y) values (?,?)",
                           &[&[&1i32, &String::from_str("godane")],
                             &[&2i32, &String::from_str("aden")]]);
            c.execute("select x, y from test");
            assert!(c.fetchmany(10).len() == 2);
        },
        Err(e)=> panic!("problem connecting to db")
        
    }

}

#[test]
fn test_execute_many(){
    let mut conn = sqlite3::connect(":memory:");
    match conn.cursor(){
        Ok(mut c) => {
            c.executescript("create table test(x, y, z)");
            c.execute_many("insert into test(x,y,z) values (?,?,?)",
                           &[&[&1i32, &String::from_str("godane"), &3.4f64],
                             &[&2i32, &String::from_str("aden")]]);
            c.execute_with_finalize("select x, y,z from test");
            //assert!(c.fetchmany(10).len() == 2);
        },
        Err(e)=> panic!("problem connecting to db")
        
    }

}
